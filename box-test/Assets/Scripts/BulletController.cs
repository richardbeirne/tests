﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour {

	private Rigidbody bullet;
	private float speed = 10;

	// Use this for initialization
	void Start () {
		bullet = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate () {
		bullet.AddRelativeForce (Vector3.forward * speed);
	}

	void OnCollisionEnter () {
		Game.Data.hitCount++;
		Debug.Log(Game.Data.hitCount);
		Destroy (gameObject);
	}
}
