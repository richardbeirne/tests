﻿using UnityEngine;
using System.Collections;

public class GunController : MonoBehaviour {

	public GameObject bullset;
	public Transform bulletSpawn;

	private float fireRate = 0.25f, lastShot;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey("space")) {
			FireBullet ();
		}
	}

	void FireBullet () {
		if (Time.time > fireRate + lastShot) {
			Instantiate (bullset, bulletSpawn.position, bulletSpawn.rotation);
			lastShot = Time.time;
		}
	}
}
