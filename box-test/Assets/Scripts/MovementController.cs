﻿using UnityEngine;
using System.Collections;

public class MovementController : MonoBehaviour {

	public float moveSpeed, turnSpeed;

	void Update () {
		Movement ();
	}

	void Movement () {
		float forwardMovement = Input.GetAxis ("Vertical") * Time.deltaTime * moveSpeed;
		float turnMovement = Input.GetAxis ("Horizontal") * Time.deltaTime * turnSpeed;

		transform.Translate (Vector3.forward * forwardMovement);
		transform.Rotate (Vector3.up * turnMovement);
	}
}